package com.example.world;

import com.example.organism.GamePiece;

/**
 * Interface that is implemented by all game board renderers.
 * @param <W>
 */
public interface GameBoardRenderer<W extends GameBoard<? extends GamePiece>> {

    /**
     * Renders the given world.
     *
     * @param world the world to render
     */
    void render(W world);

    /**
     * Prints a header.
     *
     * @param string the content of the header
     */
    void printHeader(String string);

    /**
     * Prints a divider.
     */
    void printDivider();

}

package com.example.world;

import com.example.organism.Cell;

/**
 * Renderer implementation that prints to the console.
 */
public class ConsoleCellWorldRenderer implements GameBoardRenderer<CellWorld> {

    private static final String CELL_DIVIDER = "   ";

    @Override
    public void render(CellWorld world) {
        if (world == null) {
            System.out.println("<null world>");
            return;
        }
        int width = world.getWidth();
        int height = world.getHeight();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Cell cell = world.get(x, y);
                if (cell == null) {
                    System.out.print("?" + CELL_DIVIDER);
                } else {
                    System.out.print(cell.render() + CELL_DIVIDER);
                }
            }
            System.out.println("\n");
        }
    }

    @Override
    public void printHeader(String string) {
        if (string != null) {
            System.out.println(string);
        }
        System.out.println();
    }

    @Override
    public void printDivider() {
        System.out.println();
    }
}

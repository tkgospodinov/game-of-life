package com.example.world;

import java.util.Objects;

/**
 * A utility class that represents a position on the game board.
 */
public class Position {
    private int x;
    private int y;

    /**
     * Constructs an instance.
     *
     * @param x the horizontal coordinate, a positive integer
     * @param y the vertical coordinate, a positive integer
     */
    public Position(int x, int y) {
        if (x < 0 || y < 0) {
            throw new IllegalArgumentException("The position coordinates must be positive numbers");
        }
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

}

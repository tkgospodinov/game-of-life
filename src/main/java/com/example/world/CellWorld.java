package com.example.world;

import com.example.organism.Cell;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

/**
 * A game of life cell world.
 */
public class CellWorld implements GameBoard<Cell> {

    /**
     * The maximum allowed width of the world.
     */
    private static int MAX_WIDTH = 999;

    /**
     * The maximum allowed height of the world.
     */
    private static int MAX_HEIGHT = 999;

    private static final int[] NEIGHBORS_X = {-1, -1, -1, 0, 0, 1, 1, 1};
    private static final int[] NEIGHBORS_Y = {-1, 0, 1, 1, -1, 1, 0, -1};

    private final int height;
    private final int width;

    private Cell[][] cells;

    /**
     * Constructs a cell world of the given size.
     *
     * @param width the width of the world
     * @param height the height of the world
     * @throws {@link IllegalArgumentException} if the give width and/or height are a negative number or exceed the
     * maximum allowed values
     */
    public CellWorld(int width, int height) {
        if (width <= 0 || width > MAX_WIDTH || height <= 0 || height > MAX_HEIGHT) {
            throw new IllegalArgumentException("Invalid field size");
        }
        this.height = height;
        this.width = width;
        cells = new Cell[height][width];
        initialize();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    private void initialize() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                cells[y][x] = new Cell();
            }
        }
    }

    @Override
    public Cell get(int x, int y) {
        if (x >= width || y >= height) {
            throw new IllegalArgumentException("Invalid position");
        }
        return cells[y][x];
    }

    public Set<Cell> getNeighbors(int x, int y) {
        return getNeighbors(x, y, c -> true);
    }

    @Override
    public Set<Cell> getNeighbors(int x, int y, Predicate<Cell> predicate) {
        Set<Cell> neighbors = new HashSet<>();
        for (int coord = 0; coord < NEIGHBORS_X.length; coord++) {
            int neighborX = x + NEIGHBORS_X[coord];
            int neighborY = y + NEIGHBORS_Y[coord];
            if (isSafe(neighborX, neighborY) && predicate.test(cells[neighborY][neighborX])) {
                neighbors.add(cells[neighborY][neighborX]);
            }
        }
        return neighbors;
    }

    /**
     * Checks whether the given coordinates are valid for this world.
     *
     * @param x the horizontal coordinate
     * @param y the vertical coordinate
     * @return {@code true} if the given coordinates are within the bounds of this world, and {@code false} otherwise
     */
    private boolean isSafe(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

}

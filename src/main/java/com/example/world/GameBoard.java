package com.example.world;

import com.example.organism.GamePiece;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * An interface representing a game world.
 *
 * @param <I> the game piece type this world contains
 */
public interface GameBoard<I extends GamePiece> {

    /**
     * Returns the game piece located at the specified coordinates.
     *
     * @param x the horizontal coordinate
     * @param y the vertical coordinate
     * @return the game piece found at the given coordinates
     */
    I get(int x, int y);

    /**
     * Returns the neighbors of the game piece located at the given coordinates.
     *
     * @param x the horizontal coordinate
     * @param y the vertical coordinate
     * @return the collection of neighbor game pieces
     */
    Collection<I> getNeighbors(int x, int y);

    /**
     * Returns the neighbors of the game piece located at the given coordinates that fulfill the given predicate.
     *
     * @param x the horizontal coordinate
     * @param y the vertical coordinate
     * @param predicate the predicate to check against each neighbor
     * @return the collection of neighbor game pieces that fulfill the predicate
     */
    Collection<I> getNeighbors(int x, int y, Predicate<I> predicate);

    /**
     * Returns the width of this world.
     *
     * @return the width
     */
    int getWidth();

    /**
     * Returns the height of this world.
     *
     * @return the height
     */
    int getHeight();

}

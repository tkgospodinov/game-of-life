package com.example;

import com.example.world.ConsoleCellWorldRenderer;
import com.example.world.Position;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * The main entrypoint to the application.
 */
public class Application {

    private static final int DEFAULT_WIDTH = 5;
    private static final int DEFAULT_HEIGHT = 5;
    private static final String DEFAULT_ALIVE_POSITIONS = "2,1;3,2;3,3;2,3;1,3";

    private static final String WIDTH_ARG = "width";
    private static final String HEIGHT_ARG = "height";
    private static final String ALIVE_ARG = "alive";

    public static void main(String[] args) throws Exception {
        Map<String, String> arguments = parseArguments(args);
        int width = getWidth(arguments);
        int height = getHeight(arguments);
        Set<Position> aliveCellPositions = getAliveCells(arguments, width, height);
        GameOfLife game = new GameOfLife(width, height, aliveCellPositions, new ConsoleCellWorldRenderer());
        game.renderWorld();
        acceptInput(game);
    }

    private static void acceptInput(GameOfLife game) throws IOException {
        String input = "";
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        boolean on = true;
        while(true) {
            System.out.println("<Enter> to continue; \"q\" to quit");
            input = reader.readLine();
            if (input.equalsIgnoreCase("q")) {
                System.exit(0);
            }
            game.tick();
        }
    }

    /**
     * Validates, parces and returns the processed positions of the live cells the world should be initialized with.
     *
     * @param arguments the processed arguments
     * @param width the width of the world
     * @param height the height of the world
     * @return the resulting set of positions
     */
    private static Set<Position> getAliveCells(Map<String, String> arguments, int width, int height) {
        Set<Position> result = new HashSet<>();
        String alivePositions = arguments.get(ALIVE_ARG);
        if (alivePositions == null) {
            alivePositions = DEFAULT_ALIVE_POSITIONS;
        }
        String[] rawPositionRanges = alivePositions.split(";");
        for (String rawPositionRange : rawPositionRanges) {
            String[] parts = rawPositionRange.split(",");
            if (parts.length != 2) {
                printError("Invalid position range: " + rawPositionRange);
            }
            int x = -1;
            int y = -1;
            try {
                x = Integer.parseInt(parts[0].trim());
                y = Integer.parseInt(parts[1].trim());
            } catch (NumberFormatException e) {
                printError("Invalid position range: " + rawPositionRange);
            }
            if (x < 0 || x >= width || y < 0 || y >= height) {
                printError("Position is outside of the field bounds [" + rawPositionRange + "]");
            }
            result.add(new Position(x, y));
        }
        if (result.size() > width * height) {
            printError("The number of alive cell positions exceed the possible positions on the field [" + result.size() +"]");
        }
        return result;
    }

    /**
     * Validates the width argument value and returns it. If no width has been passed, the default value is returned.
     *
     * @param arguments the processed arguments
     * @return the width value to use
     */
    private static int getWidth(Map<String, String> arguments) {
        int width = DEFAULT_WIDTH;
        String widthArg = arguments.get(WIDTH_ARG);
        String invalidWidthValueMessage = "Invalid width value [" + widthArg + "]; width should be a number between 1 and 999";
        if (widthArg == null) {
            return width;
        }
        if (widthArg.length() > 3) {
            printError(invalidWidthValueMessage);
        }
        try {
            width = Integer.parseInt(widthArg);
        } catch (NumberFormatException e) {
            printError(invalidWidthValueMessage);
        }
        if (width < 0 || width > 999) {
            printError(invalidWidthValueMessage);
        }
        return width;
    }

    /**
     * Validates the height argument value and returns it. If no height has been passed, the default value is returned.
     *
     * @param arguments the processed arguments
     * @return the height value to use
     */
    private static int getHeight(Map<String, String> arguments) {
        int height = DEFAULT_HEIGHT;
        String heightArg = arguments.get(HEIGHT_ARG);
        String invalidHeightValueMessage = "Invalid height value [" + heightArg + "]; height should be a number between 1 and 999";
        if (heightArg == null) {
            return height;
        }
        if (heightArg.length() > 3) {
            printError(invalidHeightValueMessage);
        }
        try {
            height = Integer.parseInt(heightArg);
        } catch (NumberFormatException e) {
            printError(invalidHeightValueMessage);
        }
        if (height < 0 || height > 999) {
            printError(invalidHeightValueMessage);
        }
        return height;
    }

    /**
     * Parses the command line arguments and adds them to a map.
     *
     * @param arguments the arguments to parse
     * @return the parsed and processed arguments
     */
    private static Map<String, String> parseArguments(String[] arguments) {
        Map<String, String> result = new HashMap<>();
        for (String arg : arguments) {
            arg = arg.toLowerCase();
            if (arg.length() < 3) {
                printError("Argument too short: " + arg);
            }
            if (arg.startsWith("--")) {
                arg = arg.substring(2);
                String[] parts = arg.split("=");
                if (parts.length != 2) {
                    printError("Invalid argument: " + arg);
                }
                result.put(parts[0].trim(), parts[1].trim());
            }
        }
        return result;
    }

    /**
     * Prints an error and exits.
     *
     * @param error the error to print
     */
    private static void printError(String error) {
        System.out.println(error);
        printHelp();
        System.exit(1);
    }

    /**
     * Prints instructions, listing all valid arguments and their value ranges.
     */
    private static void printHelp() {
        System.out.println("\nValid arguments:\n\n" +
                "--" + WIDTH_ARG + "=<value [1..100]>\nThe width of the game field\n\n" +
                "--" + HEIGHT_ARG + "=<value [1..100]>\nThe height of the game field\n\n" +
                "--" + ALIVE_ARG + "=<x1 [0..998]>,<y1 [0..998]>;<x2 [0..998]>,<y2 [0..998]>,...,<xn [0..998]>,<yn [0..998]>\n" +
                "The list of positions of the cells that should be marked as alive");
    }

}

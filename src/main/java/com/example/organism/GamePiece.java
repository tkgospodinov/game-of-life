package com.example.organism;

/**
 * An interface that represents a game piece.
 */
public interface GamePiece {

    /**
     * Renders this game piece to a string.
     *
     * @return the {@link String} representation of this game piece
     */
    String render();

}

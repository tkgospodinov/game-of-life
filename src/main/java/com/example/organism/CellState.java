package com.example.organism;

/**
 * Game of life cell state.
 */
public enum CellState {
    ALIVE, DEAD
}

package com.example.organism;

/**
 * A game of life cell game piece.
 */
public class Cell implements GamePiece {

    /**
     * The string representation for a live cell.
     */
    private static final String ALIVE_STRING = "O";

    /**
     * The string representation for a dead cell.
     */
    private static final String DEAD_STRING = "-";

    private CellState state;

    /**
     * Constructs a new dead cell.
     */
    public Cell() {
        this.state = CellState.DEAD;
    }

    /**
     * Constructs a new cell with the given state.
     *
     * @param state the state of the new cell
     */
    public Cell(CellState state) {
        if (state == null) {
            throw new IllegalArgumentException("The cell state must not be null");
        }
        this.state = state;
    }

    /**
     * Returns this cell's state.
     *
     * @return the {@link CellState}
     */
    public CellState getState() {
        return state;
    }

    /**
     * Sets this cell's state.
     *
     * @param state the {@link CellState} to set
     */
    public void setState(CellState state) {
        this.state = state;
    }

    @Override
    public String render() {
        if (state == CellState.ALIVE) {
            return ALIVE_STRING;
        }
        return DEAD_STRING;
    }
}

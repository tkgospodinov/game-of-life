package com.example;

import com.example.organism.Cell;
import com.example.organism.CellState;
import com.example.rule.GameOfLifeRule;
import com.example.rule.Rule;
import com.example.world.CellWorld;
import com.example.world.GameBoard;
import com.example.world.Position;
import com.example.world.GameBoardRenderer;

import java.util.Set;

/**
 * Game of life implementation that orchestrates the game, initializing the board, applies rules and renders the world.
 */
public class GameOfLife {

    private final Set<Position> aliveCells;

    private final GameBoardRenderer<CellWorld> renderer;

    private CellWorld world;

    private Rule<GameBoard<Cell>> ruleSet;

    private int generation;

    /**
     * Creates a new instance.
     *
     * @param width the width of the world
     * @param height the height of the world
     * @param renderer the renderer to use to render the world
     */
    public GameOfLife(int width, int height, GameBoardRenderer<CellWorld> renderer) {
        this(width, height, null, renderer);
    }

    /**
     * Creates a new instance.
     *
     * @param width the width of the world
     * @param height the height of the world
     * @param aliveCells the positions of the cells that should be initialized as live
     * @param renderer the renderer implementation that should be used to render the world
     */
    public GameOfLife(int width, int height, Set<Position> aliveCells, GameBoardRenderer<CellWorld> renderer) {
        this.world = new CellWorld(width, height);
        this.ruleSet = new GameOfLifeRule();
        this.aliveCells = aliveCells;
        this.renderer = renderer;
        initialize();
    }

    /**
     * Marks the cells in the given positions as live.
     */
    private void initialize() {
        if (aliveCells != null) {
            for (Position aliveCellPosition : aliveCells) {
                if (aliveCellPosition != null) {
                    world.get(aliveCellPosition.getX(), aliveCellPosition.getY()).setState(CellState.ALIVE);
                }
            }
        }
        generation = 1;
    }

    /**
     * Moves to the next generation, applying the rules and rendering the world.
     */
    public void tick() {
        ruleSet.apply(world);
        generation++;
        renderWorld();
    }

    /**
     * Renders the world.
     */
    public void renderWorld() {
        renderer.printHeader("Generation: " + generation);
        renderer.render(world);
        renderer.printDivider();
    }

}

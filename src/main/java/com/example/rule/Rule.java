package com.example.rule;

import com.example.organism.GamePiece;
import com.example.world.GameBoard;

/**
 * Game rule that can be applied to a game world.
 *
 * @param <I> the game world type this rule can be applied to
 */
public interface Rule<I extends GameBoard<? extends GamePiece>> {

    /**
     * Applies this rule to a game world.
     *
     * @param world the game world to apply the rule to
     */
    void apply(I world);

}

package com.example.rule;

import com.example.organism.Cell;
import com.example.organism.CellState;
import com.example.world.GameBoard;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * The game of life rule.
 *
 * Any live cell with fewer than two live neighbours dies, as if caused by under­population.
 * Any live cell with two or three live neighbours lives on to the next generation.
 * Any live cell with more than three live neighbours dies, as if by overcrowding.
 * Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
 */
public class GameOfLifeRule implements Rule<GameBoard<Cell>> {

    /**
     * Underpopulation threshold, live cells whose number of neighbors is equal or less than this threshold die.
     */
    private static final int UNDERPOPULATION_THRESHOLD = 1;

    /**
     * Overpopulation threshold, live cells whose number of neighbors is equal or greater than this threshold die.
     */
    private static final int OVERPOPULATION_THRESHOLD = 4;

    /**
     * Reproduction number, deadcells whose number of neighbors are equal to this number will become live.
     */
    private static final int REPRODUCTION_NUMBER = 3;

    @Override
    public void apply(GameBoard<Cell> world) {
        Map<Cell, CellState> toUpdate = getCellsToUpdate(world);
        for(Map.Entry<Cell, CellState> e : toUpdate.entrySet()) {
            e.getKey().setState(e.getValue());
        }
    }

    /**
     * Returns the map of cells to the new cell state that needs to be applied to each, after applying the game of
     * life rule.
     *
     * @param world the world to evaluate
     * @return a map of cells to their new cell states
     */
    private Map<Cell, CellState> getCellsToUpdate(GameBoard<Cell> world) {
        Map<Cell, CellState> toUpdate = new HashMap<>();
        for (int x = 0; x < world.getWidth(); x++) {
            for (int y = 0; y < world.getHeight(); y++) {
                Cell currentCell = world.get(x, y);
                Collection<Cell> aliveNeighbors = world.getNeighbors(x, y, n -> n.getState() == CellState.ALIVE);
                int aliveCount = aliveNeighbors.size();
                switch (currentCell.getState()) {
                    case ALIVE:
                        if (aliveCount <= UNDERPOPULATION_THRESHOLD || aliveCount >= OVERPOPULATION_THRESHOLD) {
                            toUpdate.put(currentCell, CellState.DEAD);
                        }
                        break;
                    case DEAD:
                        if (aliveCount == REPRODUCTION_NUMBER) {
                            toUpdate.put(currentCell, CellState.ALIVE);
                        }
                        break;
                }
            }
        }
        return toUpdate;
    }

}

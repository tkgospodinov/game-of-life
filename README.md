# Game of Life

A java implementation of Conway's Game of Life.

## Definition

The universe of the Game of Life is an infinite two­dimensional orthogonal grid of squarecells, each of which
is in one of two possible states, alive or dead. Every cell interacts with its eight neighbours, which are the
cells that are horizontally, vertically, or diagonally adjacent.

## Rules

At each step in time, the following transitions occur:
1. Any live cell with fewer than two live neighbours dies, as if caused by under­population.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overcrowding.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

The initial pattern constitutes the seed of the system. The first generation is created by applying the above
rules simultaneously to every cell in the seed—births and deaths occur simultaneously, and the discrete
moment at which this happens is sometimes called a tick (in other words, each generation is a pure function
of the preceding one). The rules continue to be applied repeatedly to create further generations.

## Launching the game

1. Package a jar

```
./gradlew jar
```

2. Launch the jar

```
java -jar build/libs/game-of-life-1.0-SNAPSHOT.jar
```

The jar can be launched with a few arguments:

1. `--width=<number>` the value should be between 0 and 999, if none is specified, the default (5) will be used
2. `--height=<number>` the value should be between 0 and 999, if none is specified, the default (5) will be used
3. `--alive=<pairs>` the coordinate of the cells that should be marked as live in the very first generation.
 The format is one to many coordinate pairs (`<x>,<y>`) divided by semicolons (`<x1>,<y1>;<x2>,<y2>;...;<xn>,<yn>`).
 If none is specified, the default (`2,1;3,2;3,3;2,3;1,3`) will be used.